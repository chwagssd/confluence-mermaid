# confluence-mermaid

A plugin for Confluence Cloud to allow diagrams authored in the Mermaid markup language.